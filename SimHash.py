import hashlib
import sys
import numpy as np

length = 128


def read_from_stdin():
    N = int(sys.stdin.readline().rstrip('\n'))
    texts = [sys.stdin.readline() for i in range(N)]
    Q = int(sys.stdin.readline().rstrip('\n'))
    queries = [sys.stdin.readline() for i in range(Q)]
    return N, Q, texts, queries


def simhash(text):
    sh = [0 for i in range(length)]
    for txt in text.split():
        binary = bin(int(hashlib.md5(txt.encode()).hexdigest(), 16)).lstrip('0b').zfill(length)
        sh = [sh[i] + (1 if binary[i] == '1' else -1) for i in range(length)]
    return np.where(np.array(sh) >= 0, 1, 0)


def hamming_distance(s1, s2):
    return np.count_nonzero(s1 != s2)


N, Q, texts, queries = read_from_stdin()
simhashes = [simhash(text) for text in texts]

for query in queries:
    splits = query.split()
    I, K = int(splits[0]), int(splits[1])
    s1 = simhashes[I]
    num = 0
    for i, s2 in enumerate(simhashes):
        if hamming_distance(s1, s2) <= K and i != I:
            num += 1
    print(num)
