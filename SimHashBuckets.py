import sys
import numpy as np
import hashlib

length = 128
bands = 8
rows = 16


def read_from_stdin():
    N = int(sys.stdin.readline().rstrip())
    texts = [sys.stdin.readline().rstrip() for i in range(N)]
    Q = int(sys.stdin.readline().rstrip())
    queries = [sys.stdin.readline().rstrip() for i in range(Q)]
    return N, Q, texts, queries


def simhash(text):
    sh = [0 for i in range(length)]
    for txt in text.split():  # re.split('\s+', text):
        binary = bin(int(hashlib.md5(txt.encode()).hexdigest(), 16)).lstrip('0b').zfill(length)
        sh = [sh[i] + (1 if binary[i] == '1' else -1) for i in range(length)]
    return np.where(np.array(sh) >= 0, 1, 0)


def hash_to_int(band, hash):
    return ''.join(str(x) for x in hash[(rows * band):(rows * (band + 1))])


def lsh(simhashes, N):
    candidates = dict()
    for band in range(bands):
        buckets = dict()
        for i in range(N):
            value = hash_to_int(band, simhashes[i])
            texts_in_bucket = [i]
            if value in buckets:
                texts_in_bucket += buckets[value]
                for j in texts_in_bucket:
                    if i not in candidates:
                        candidates[i] = set()
                    candidates[i].add(j)
                    if j not in candidates:
                        candidates[j] = set()
                    candidates[j].add(i)
            buckets[value] = texts_in_bucket
    return candidates


def hamming_distance(s1, s2):
    return np.count_nonzero(s1 != s2)


N, Q, texts, queries = read_from_stdin()
simhashes = [simhash(text) for text in texts]
candidates = lsh(simhashes, N)

for query in queries:
    splits = query.split()
    I, K = int(splits[0]), int(splits[1])
    num = 0
    for i in candidates[I]:
        if hamming_distance(simhashes[I], simhashes[i]) <= K and i != I:
            num += 1
    print(num)
